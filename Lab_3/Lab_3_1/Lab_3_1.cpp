#include<cassert>
#include<memory>
#include<iomanip>
#include<iostream>
using namespace std;

typedef unique_ptr<uint8_t[]> byteArr_t;
typedef unique_ptr<double[]> doubleArr_t;

bool arePythagorianNumbers(double a, double b, double c) {
	return (((a * a + b * b) == (c * c)) || ((c * c + b * b) == (a * a)) || ((a * a + c * c) == (b * b)));
}

byteArr_t encryptArray(const double arrayToEncrypt[], size_t sizeOfArray, uint8_t key) {
	size_t sizeOfUint8tArray = sizeOfArray * sizeof(double);
	byteArr_t encryptedCharArray = make_unique<uint8_t[]>(sizeOfUint8tArray);
	auto charArray = reinterpret_cast<const uint8_t*>(arrayToEncrypt);
	for (size_t i = 0; i < sizeOfUint8tArray; ++i) {
		encryptedCharArray[i] = charArray[i] ^ key;
	}
	return encryptedCharArray;
}


doubleArr_t dencryptArray(const uint8_t arrayToDencrypt[], size_t sizeOfArray, uint8_t key) {
	const size_t sizeOfDoubleArray = sizeOfArray * sizeof(double);
	auto result = make_unique<double[]>(sizeOfDoubleArray);
	auto p = reinterpret_cast<uint8_t*>(result.get());
	for (size_t i = 0; i < sizeOfDoubleArray; ++i) {
		p[i] = arrayToDencrypt[i] ^ key;
	}
	return result;
}

bool areCorrectNumbers(double a, double b, double c) {
	const double minNumber = 0.01;
	const double maxNumber = 100000.0;
	return ((a > minNumber) && (b > minNumber) && (c > minNumber) && (a < maxNumber) && (b < maxNumber) && (c < maxNumber));
}

uint8_t findTheKey(const uint8_t numbers[], size_t sizeOfArray) {
	for (int j = 0; j < 256; ++j) {
		uint8_t key = j;
		doubleArr_t result = dencryptArray(numbers, sizeOfArray, key);
	
		bool areRightNumbers = true;
		auto p = result.get();
		auto pEnd = p + sizeOfArray;
		while (p != pEnd) {
			auto a = *p++;
			auto b = *p++;
			auto c = *p++;

			if (p == NULL) {
				areRightNumbers = false;
				break;
			}

			if (!areCorrectNumbers(a, b, c)) {
				areRightNumbers = false;
				break;
			}

			if (!arePythagorianNumbers(a, b, c)) {
				areRightNumbers = false;
				break;
			}
		}
		if (areRightNumbers) {
			return key;
		}
	}
	return NULL;
}

int main(int argc, char* argv[])
{
	const double arr0[] { 6.0, 8.0, 10.0 };
	const double arr1[] { 31.4, 31.4, 31.4};
	const double arr2[] { 5, 4, 3, 2};
	const size_t numberOfElements0 = sizeof(arr0) / sizeof(*arr0);
	const size_t numberOfElements1 = sizeof(arr1) / sizeof(*arr1);
	const size_t numberOfElements2 = sizeof(arr2) / sizeof(*arr2);

	byteArr_t arr0_b = encryptArray(arr0, numberOfElements0 * sizeof(double), 'A');
	assert(findTheKey(arr0_b.get(), numberOfElements0) == 'A');
	
	byteArr_t arr1_b = encryptArray(arr1, numberOfElements1 * sizeof(double), 'M');
	assert(findTheKey(arr1_b.get(), numberOfElements1) == NULL);
	
	byteArr_t arr2_b = encryptArray(arr2, numberOfElements2 * sizeof(double), 'B');
	assert(findTheKey(arr2_b.get(), numberOfElements2) == NULL);
	return 0;
}

