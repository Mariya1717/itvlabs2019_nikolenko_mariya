#include<cassert>
#include<iostream>
#include<algorithm>
#include<iomanip>
#include<memory>
using namespace std;

typedef unique_ptr<uint8_t[]> byteArr_t;
typedef unique_ptr<double[]> doubleArr_t;

byteArr_t encryptArray(const double arrayToEncrypt[], size_t sizeOfArray, uint8_t key) {
	size_t sizeOfUint8tArray = sizeOfArray * sizeof(double);
	byteArr_t encryptedCharArray = make_unique<uint8_t[]>(sizeOfUint8tArray);
	auto charArray = reinterpret_cast<const uint8_t*>(arrayToEncrypt);
	for (size_t i = 0; i < sizeOfUint8tArray; ++i) {
		encryptedCharArray[i] = charArray[i] ^ key;
	}
	return encryptedCharArray;
}


doubleArr_t dencryptArray(const uint8_t arrayToDencrypt[], size_t sizeOfArray, uint8_t key) {
	const size_t sizeOfDoubleArray = sizeOfArray * sizeof(double);
	auto result = make_unique<double[]>(sizeOfDoubleArray);
	auto p = reinterpret_cast<uint8_t*>(result.get());
	for (size_t i = 0; i < sizeOfDoubleArray; ++i) {
		p[i] = arrayToDencrypt[i] ^ key;
	}
	return result;
}

bool isEqualDoubleArrays(const double array1[], const double array2[], size_t sizeOfArray) {
	const double epsilon = 0.00001;
	for (size_t i = 0; i < sizeOfArray; ++i) {
		if (fabs(array1[i] - array2[i]) > max(fabs(array1[i]), fabs(array2[i]))*epsilon) {
			return false;
		}
	}
	return true;
}

int main(int argc, char* argv[])
{
	const double arr[] { 3.0, 4.0, 0.9876543, 5.0, -9.0, -23.0765 };
	const size_t numberOfElements = sizeof(arr) / sizeof(*arr);
	
	uint8_t key = 'b';
	byteArr_t enArray = encryptArray(arr, numberOfElements, key);
	doubleArr_t deArray = dencryptArray(enArray.get(), numberOfElements, key);

	assert(isEqualDoubleArrays(arr, deArray.get(), numberOfElements) == true, "Wrong");
	return 0;
}



