#include<cassert>
#include<iostream>
#include<string>
#include <library.h>
#pragma comment(lib, "library.lib")
using namespace std;

string convertFloatNumber(float number){
	return convertNumberToMoneyFormat(number);
}

int main(int argc, char* argv[])
{
	setlocale(LC_ALL, "ru");
	cout << "�������������� ����� � �������� ������" << endl;
	assert(convertFloatNumber(0.12f) == "0 ���. 12 ���.", "wrong");
	assert(convertFloatNumber(456.1f) == "456 ���. 10 ���.", "wrong");
	assert(convertFloatNumber(132.0f) == "132 ���. 0 ���.", "wrong");
	cout << "��������� - " << convertFloatNumber(enterFloatNumber("������� �����")) << endl;
	system("pause");
	return 0;
}