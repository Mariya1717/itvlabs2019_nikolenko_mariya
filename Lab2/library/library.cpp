#include<string>
#include<iostream>
#include<math.h>
using namespace std;

string convertNumberToMoneyFormat(float number) {
	int rubles = static_cast<int>(number);
	float numberMultipliedBy100 = number * 100.0f;
	int roundedNumber = round(numberMultipliedBy100);
	int kopecks = roundedNumber % 100;
	string moneyFormat = to_string(rubles) + " ���. ";
	moneyFormat += to_string(kopecks) + " ���.";
	return moneyFormat;
}

float enterFloatNumber(const string& name) {
	float number;
	cout << "������� " << name << " - ";
	while (!(cin >> number) || (cin.peek() != '\n')) {
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
		cout << "�� ���������� �����. ������� " << name << " ��� ���: ";
	}
	return number;
}

bool isEqualFloatNumbers(float number1, float number2) {
	const float epsilon = 0.00001f;
	return (fabs(number1 - number2) < epsilon);
}