#pragma once
#include<string>

std::string convertNumberToMoneyFormat(float number);
float enterFloatNumber(const std::string& name);
bool isEqualFloatNumbers(float number1, float number2);
