#include<cassert>
#include<math.h>
#include<iostream>
#include <library.h>
#pragma comment(lib, "library.lib")
using namespace std;

//solution of quation ax2 + bx + c = 0
bool isRootsOfFirstQuation(float a, float b, float c, float x) {
	return isEqualFloatNumbers((a*pow(x, 2) + b*x + c), 0.0f);
}

//solution of quation mx + n = 0
bool isRootOfSecondQuation(float m, float n, float x) {
	return isEqualFloatNumbers((m*x + n), 0.0f);
}

bool isRootOfQuation(float a, float b, float c, float m, float n, float d) {
	return (isRootsOfFirstQuation(a, b, c, d) ^ isRootOfSecondQuation(m, n, d));
}

int main(int argc, char* argv[])
{
	setlocale(LC_ALL, "ru");
	assert(isRootOfQuation(2.0f, 3.0f, 1.0f, 2.0f, 2.0f, 45.0f) == 0, "Wrong");
	assert(isRootOfQuation(2.0f, 3.0f, 1.0f, 2.0f, 43.0f, -1.0f) == 1, "Wrong");
	assert(isRootOfQuation(2.0f, 3.0f, 1.0f, 2.0f, 2.0f, -1.0f) == 0, "Wrong");
	assert(isRootOfQuation(2.0f, 3.0f, 1.0f, -2.0f, 12.0f, 6.0f) == 1, "Wrong");
	cout << "��������� - " << isRootOfQuation(enterFloatNumber("a"), enterFloatNumber("b"), enterFloatNumber("c"),
		enterFloatNumber("m"), enterFloatNumber("n"), enterFloatNumber("d")) << endl;
	system("pause");
	return 0;
}



