#include <cassert>
#include <iostream>
#include <library.h>
#pragma comment(lib, "library.lib")
using namespace std;

string calculatingTheCostOfTheTrip(float distance, float fuelConsumptionFor100km, float fuelCost) {
	if (distance < 0 || fuelConsumptionFor100km < 0 || fuelCost < 0) {
		return "";
	}
	const float kmForFlueConsumption = 100.0f;
	const float numberOfTrips = 2.0f;
	float fuelConsumptionForDistance = distance * fuelConsumptionFor100km / kmForFlueConsumption;
	float cost = fuelConsumptionForDistance * fuelCost * numberOfTrips;
	return convertNumberToMoneyFormat(cost);
}

int main(int argc, char* argv[]) {
	setlocale(LC_ALL, "ru");
	cout << "���������� ��������� ������� �� ����." << endl;
	assert(calculatingTheCostOfTheTrip(67.0f, 8.5f, 23.7f) == "269 ���. 94 ���.", "Wrong");
	assert(calculatingTheCostOfTheTrip(-67.0f, 8.5f, 23.7f).empty() == true, "Wrong");
	assert(calculatingTheCostOfTheTrip(67.0f, -8.5f, -23.7f).empty() == true, "Wrong");
	assert(calculatingTheCostOfTheTrip(-67.0f, -8.5f, -23.7f).empty() == true, "Wrong");
	assert(calculatingTheCostOfTheTrip(0, -0.5f, 0.7f).empty() == true, "Wrong");
	cout << "��������� - " << calculatingTheCostOfTheTrip(enterFloatNumber("���������� �� ����"), enterFloatNumber("������ ������� (� �� 100 ��)"), enterFloatNumber("���� ����� ������� (���.)")) << endl;
	system("pause");
	return 0;
}
