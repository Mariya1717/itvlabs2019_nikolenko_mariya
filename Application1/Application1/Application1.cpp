// Application1.cpp: определяет точку входа для консольного приложения.
//
#include <cassert> 
#include <math.h>
#include <iostream>
using namespace std;


unsigned int sumOf3numbersOfFractionalPart(float number) {
	if (number < 0) {
		number = abs(number);
	}
	float numberMultipliedBy1000 = number * 1000.0f;
	int numberOfFractionalPart = static_cast<int>(numberMultipliedBy1000) % 1000;
	int firstNumber = numberOfFractionalPart / 100,
		secondNumber = (numberOfFractionalPart % 100) / 10,
		thirdNumber = numberOfFractionalPart % 10;
	int sum = firstNumber + secondNumber + thirdNumber;
	return sum;
}


int main(int argc, char* argv[])
{
	struct testData_t
	{
		float d; int sum;
	};
	const testData_t testData[] = { { 0.123f, 6 }, { -4.2141f, 7 }, { 3.0f, 0 }, { 6.999f, 27 }, { -5.003f, 3 },
	{ 5.98756f, 24 }, { 67.34f, 7 }, { 1.2f, 2 }, { -9.0f, 0 }
	};
	const size_t numberOfTests = sizeof(testData) / sizeof(testData[0]);
	for (int i = 0; i < numberOfTests; ++i) {
		assert(sumOf3numbersOfFractionalPart(testData[i].d) == testData[i].sum, "Wrong number");
	}

	return 0;
}

